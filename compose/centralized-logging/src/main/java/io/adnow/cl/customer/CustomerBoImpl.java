package io.adnow.cl.customer;

import io.adnow.cl.aspect.Loggable;
import org.springframework.stereotype.Component;

/**
 * // TODO Write javadoc.
 *
 * @author Matthieu Honel <matthieu.honel@adnow.fr>
 * @since 2016-07-25
 */
@Component("CustomerBo")
public class CustomerBoImpl implements CustomerBo {

    @Loggable
    public void addCustomer(){
        System.out.println("addCustomer() is running ");
    }

    @Loggable
    public String addCustomerReturnValue(){
        System.out.println("addCustomerReturnValue() is running ");
        return "abc";
    }

    public void addCustomerThrowException() throws Exception {
        System.out.println("addCustomerThrowException() is running ");
        throw new Exception("Generic Error");
    }

    public void addCustomerAround(String name){
        System.out.println("addCustomerAround() is running, args : " + name);
    }
}