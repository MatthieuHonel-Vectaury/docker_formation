package io.adnow.cl.hello;

import io.adnow.cl.aspect.Loggable;
import io.adnow.cl.customer.CustomerBo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/hello")
public class HelloController implements ApplicationContextAware {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private ApplicationContext appContext;

    @Autowired
    private GreetingFactoryService awGreetingFactoryService;

    @Loggable
    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public Greeting greeting2(
            @RequestParam(value = "name", required = false, defaultValue = "Stranger") String name
    ) {
        return createGreetingFromFactory(name);
    }

    @Loggable
    public Greeting createGreetingFromFactory(final String name) {
        Greeting g = awGreetingFactoryService.createGreeting(counter.incrementAndGet(), String.format(template, name));
        return g;
    }






    @Loggable
    @RequestMapping(value = "/customer")
    public CustomerBo customer() {
        CustomerBo customer = (CustomerBo) appContext.getBean("CustomerBo");
        customer.addCustomer();
        customer.addCustomerReturnValue();
        return customer;
    }

    @Override
    @Loggable
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }
}