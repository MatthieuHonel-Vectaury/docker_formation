package io.adnow.cl.customer;

/**
 * // TODO Write javadoc.
 *
 * @author Matthieu Honel <matthieu.honel@adnow.fr>
 * @since 2016-07-25
 */
public interface CustomerBo {

    void addCustomer();

    String addCustomerReturnValue();

    void addCustomerThrowException() throws Exception;

    void addCustomerAround(String name);
}
