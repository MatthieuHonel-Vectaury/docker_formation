package io.adnow.cl.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * // TODO Write javadoc.
 *
 * @author Matthieu Honel <matthieu.honel@adnow.fr>
 * @since 2016-07-22
 */
@Aspect
@Component
public class MethodLoggerAspect {

    @Around("execution(* *(..)) && @annotation(io.adnow.cl.aspect.Loggable)")
    static public Object around(ProceedingJoinPoint point) throws Throwable {
        //LoggerFactory.getLogger("MethodLoggerAspect").error("@Around(@Loggable)) {}", point);
        long start = System.nanoTime();
        Object result = point.proceed();
        long end = System.nanoTime() - start;

        final Method method = MethodSignature.class.cast(point.getSignature()).getMethod();
        final Loggable annotation = method.getAnnotation(Loggable.class);
        final Logger logger = getLogger(method, annotation);

        logMethod(logger, annotation, "#{}({}): {} in {}ns", method.getName(), point.getArgs(), result, end);

        return result;
    }

    static private void logMethod(final Logger logger, final Loggable annotation, final String message, final Object... arguments) {
        switch (annotation.value()) {
            case Loggable.TRACE:
                logger.trace(message, arguments);
                break;
            case Loggable.DEBUG:
                logger.debug(message, arguments);
                break;
            case Loggable.INFO:
                logger.info(message, arguments);
            default:
                break;
            case Loggable.WARN:
                logger.warn(message, arguments);
                break;
            case Loggable.ERROR:
                logger.error(message, arguments);
                break;
        }
    }

    static private Logger getLogger(final Method method, final Loggable annotation) {
        Logger logger;
        final String name = annotation == null ? "" : annotation.name();
        if (!name.isEmpty()) {
            logger = LoggerFactory.getLogger(name);
        } else {
            logger = LoggerFactory.getLogger(method.getDeclaringClass());
        }
        return logger;
    }

}
