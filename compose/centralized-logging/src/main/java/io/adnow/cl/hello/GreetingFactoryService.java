package io.adnow.cl.hello;

import io.adnow.cl.aspect.Loggable;
import org.springframework.stereotype.Component;

@Component
public class GreetingFactoryService {

    @Loggable
    public Greeting createGreeting(final long id, final String content) {
        return new Greeting(id, content);
    }
}
