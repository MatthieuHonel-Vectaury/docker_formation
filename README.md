Docker Formation
===

# Table of Contents
- [Basic commands](#markdown-header-basic-commands)
- [ENTRYPOINT vs CMD](#markdown-header-entrypoint-vs-cmd)
- [ADD vs COPY](#markdown-header-add-vs-copy)
- [CMD shell vs exec](#markdown-header-cmd-shell-vs-exec)
- [VOLUME](#markdown-header-volume)
- [Docker-compose](#markdown-header-docker-compose)

## Basic commands [↑](#markdown-header-docker-formation)
- `docker build`: This will read the Dockerfile from the directory specified in PATH.  It also sends any other files and directories found in the current directory to the Docker daemon. The contents of this directory would be used by ADD commands found within the Dockerfile.
    **Warning, this will send a lot of data to the Docker daemon depending on the contents of the current directory. The build is run by the Docker daemon, not by the CLI, so the whole context must be transferred to the daemon.**  The Docker CLI reports "Sending build context to Docker daemon" when the context is sent to the daemon.
    When  the  URL  to  a tarball archive or to a single Dockerfile is given, no context is sent from the client to the Docker daemon. In this case, the Dockerfile at the root of the archive and the rest of the archive will get used as the context of the build. **When a Git repository is set as the URL, the repository is cloned locally and then sent as the context.**

- `docker create`: Creates a writeable container layer over the specified image and prepares it for running the specified command. The container ID is then printed to STDOUT. This is similar to docker run -d except the container is never started. You can then use the `docker start <container_id>` command to start the  container at any point
- `docker start`: Starts one or more containers
- `docker run`: ⋍ `docker create` + `docker start`
- `docker stop`: Stop a container (Send SIGTERM, and then SIGKILL after grace period)

## ENTRYPOINT vs CMD [↑](#markdown-header-docker-formation)
```
$ cd entrypoint-cmd/
$ ./my-script.sh
It is now Mon Jul 18 17:18:43 CEST 2016.
No command to execute...
Usage: ./my-script.sh your command and arguments

$ ./my-script.sh echo 'Hello World !'
It is now Mon Jul 18 17:19:32 CEST 2016.
Executing command: echo Hello World !
In 3...2...1...

Hello World !

The command has been executed !

$ docker build -t form-cmd -f Dockerfile-cmd .
Sending build context to Docker daemon 4.096 kB
Step 1 : FROM alpine
Step 2 : COPY my-script.sh /
Step 3 : RUN chmod +x /my-script.sh
Step 4 : CMD /my-script.sh echo Hello World!
Successfully built d783849c20c2

$ docker run form-cmd
It is now Mon Jul 18 15:21:15 UTC 2016.
Executing command: echo Hello World!
In 3...2...1...

Hello World!

The command has been executed !

$ docker run form-cmd echo "Salut Monde" #ouille
Salut Monde

$ docker build -t form-entrypoint -f Dockerfile-entrypoint .
Sending build context to Docker daemon 4.096 kB
Step 1 : FROM alpine
Step 2 : COPY my-script.sh /
Step 3 : RUN chmod +x /my-script.sh
Step 4 : ENTRYPOINT /my-script.sh
Step 5 : CMD echo Hello World!
Successfully built 6c8c9587cfaa

$ docker run form-entrypoint echo "Salut Monde"
It is now Mon Jul 18 15:23:28 UTC 2016.
Executing command: echo Salut Monde
In 3...2...1...

Salut Monde

The command has been executed !
```

## ADD vs COPY [↑](#markdown-header-docker-formation)
```
$ cd add-copy/
$ tar tzf data.tgz
./
./file3
./file5
./file2
./file1
./file4

$ docker build -t form-add-copy .
Sending build context to Docker daemon 3.072 kB
Step 1 : FROM alpine
Step 2 : RUN mkdir -p /data-add
Step 3 : RUN mkdir -p /data-copy
Step 4 : RUN mkdir -p /data-add-http
Step 5 : RUN mkdir -p /data-copy-http
Step 6 : ADD data.tgz /data-add/
Step 7 : COPY data.tgz /data-copy/
Step 8 : ADD http://matthieu.honel.fr/data.tgz /data-add-http/
Downloading [==================================================>]    178 B/178 B
Step 9 : ENTRYPOINT /bin/sh -c for F in '/data-add' '/data-copy' '/data-add-http' '/data-copy-http'; do echo $F:; ls -al $F; done
Successfully built 0fb9527080f8

$ docker run form-add-copy
/data-add:
file1
file2
file3
file4
file5
/data-copy:
data.tgz
/data-add-http:
data.tgz
/data-copy-http:
```

## CMD shell vs exec [↑](#markdown-header-docker-formation)
### Shell:
```
$ cd cmd-shell-exec/
$ docker build -t form-cmd-shell -f Dockerfile-shell .
Sending build context to Docker daemon 3.072 kB
Step 1 : FROM alpine
Step 2 : CMD ping localhost
Successfully built b04a09f0ccd0

$ docker run form-cmd-shell
PING localhost (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: seq=0 ttl=64 time=0.049 ms
64 bytes from 127.0.0.1: seq=1 ttl=64 time=0.044 ms
64 bytes from 127.0.0.1: seq=2 ttl=64 time=0.066 ms
...
```
In another terminal:
```
$ docker ps -l
CONTAINER ID  IMAGE           COMMAND                        CREATED         STATUS
3a6b66435b21  form-cmd-shell  "/bin/sh -c 'ping localhost'"  14 seconds ago  Up 13 seconds

$ docker exec -it 3a6b66435b21 /bin/sh
/ # ps aux
PID   USER     TIME   COMMAND
    1 root       0:00 /bin/sh -c ping localhost
    7 root       0:00 ping localhost
    8 root       0:00 /bin/sh
   13 root       0:00 ps aux
/ # exit

$ docker stop 3a6b66435b21
3a6b66435b21
```
In the first terminal, you can see the container is stopped (no ping output anymore):
```
...
64 bytes from 127.0.0.1: seq=30 ttl=64 time=0.045 ms
64 bytes from 127.0.0.1: seq=31 ttl=64 time=0.042 ms
$
```

### Exec:
```
$ docker build -t form-cmd-exec -f Dockerfile-exec .
Sending build context to Docker daemon 3.072 kB
Step 1 : FROM alpine
Step 2 : CMD ping localhost
Successfully built c4485208c58f

$ docker run form-cmd-exec
PING localhost (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: seq=0 ttl=64 time=0.039 ms
64 bytes from 127.0.0.1: seq=1 ttl=64 time=0.041 ms
64 bytes from 127.0.0.1: seq=2 ttl=64 time=0.047 ms
...
```
In another terminal:
```
$ docker ps -l
CONTAINER ID  IMAGE           COMMAND           CREATED         STATUS
d9e8d73f7e92  form-cmd-exec   "ping localhost"  14 seconds ago  Up 14 seconds

$ docker exec -it d9e8d73f7e92 /bin/sh
/ # ps aux
PID   USER     TIME   COMMAND
    1 root       0:00 ping localhost
    7 root       0:00 /bin/sh
   12 root       0:00 ps aux
/ # exit
$ docker stop d9e8d73f7e92
d9e8d73f7e92
```
In the first terminal, you can see the container is stopped (no ping output anymore):
```
64 bytes from 127.0.0.1: seq=128 ttl=64 time=0.049 ms
64 bytes from 127.0.0.1: seq=129 ttl=64 time=0.041 ms
$ 
```

## VOLUME [↑](#markdown-header-docker-formation)
```
$ cd volume/
$ tree site/
site/
└── index.php
0 directories, 1 file

$ docker build -t form-volume .
Sending build context to Docker daemon 6.144 kB
Step 1 : FROM php:5.6-cli
Step 2 : ADD site /site/www
Step 3 : VOLUME /site/www
Step 4 : ENTRYPOINT php -S 0.0.0.0:3000 -t
Step 5 : CMD /site/www
Successfully built e5e2f66a4b5a

$ docker run form-volume
```
In another terminal:
```
$ docker exec -it 974de24bfc1b ls -1 /site/www
index.php
```

In another terminal:
```
$ su -
root $ docker inspect 974de24bfc1b | grep -A10 Mounts
        "Mounts": [
            {
                "Name": "d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09",
                "Source": "/var/lib/docker/volumes/d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09/_data",
                "Destination": "/site/www",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            }
        ],

root $ tree /var/lib/docker/volumes/d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09/_data
/var/lib/docker/volumes/d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09/_data
└── index.php
0 directories, 1 file

root $ docker inspect 974de24bfc1b | grep IPAddress
            "IPAddress": "172.17.0.2",
```
In a browser, open [http://172.17.0.2:3000/](http://172.17.0.2:3000/).
In the root terminal:
```
root $ tree /var/lib/docker/volumes/d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09/_data
/var/lib/docker/volumes/d0866a959f8ad81aed37b7fe5f3a707a391677d9fef758461e4d45d60eb74d09/_data
├── index.php
└── logs.txt
0 directories, 2 files
```

Share the volume with another container:
```
$ docker run --volumes-from 974de24bfc1b ubuntu:latest ls -1 /site/www
index.php
logs.txt
```

## Docker-compose [↑](#markdown-header-docker-formation)
```
$ cd compose/
$ centralized-logging/gradlew -b centralized-logging/build.gradle build
BUILD SUCCESSFUL

Total time: 11.84 secs
$ docker-compose up --build
Removing compose_centralizedlogging_1
Building centralizedlogging
Step 1 : FROM java:8-jre-alpine
 ---> 6a8b77287171
Step 2 : COPY build/libs/centralized-logging-0.0.1-SNAPSHOT.jar /centralized-logging.jar
 ---> Using cache
 ---> 11f6edbf8e10
Step 3 : ENTRYPOINT java
 ---> Using cache
 ---> a39e66af04e2
Step 4 : CMD -jar /centralized-logging.jar
 ---> Using cache
 ---> 25b7c2f4d343
Successfully built 25b7c2f4d343
compose_elk_1 is up-to-date
compose_kibana_1 is up-to-date
compose_logstash_1 is up-to-date
Recreating bb7b2e182f69_compose_centralizedlogging_1
Attaching to compose_elk_1, compose_kibana_1, compose_logstash_1, compose_centralizedlogging_1
[...]
Gracefully stopping... (press Ctrl+C again to force)
Stopping compose_centralizedlogging_1 ... done
Stopping compose_logstash_1 ... done
Stopping compose_kibana_1 ... done
Stopping compose_elk_1 ... done
```