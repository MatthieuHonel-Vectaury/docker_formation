#!/bin/sh

echo "It is now $(date)."
C="$@"
if [ ! -n "$C" ]
then
    echo "No command to execute...";
    echo "Usage: $0 your command and arguments"
    exit 0; 
fi

echo "Executing command: $@"
echo -n "In 3..."
sleep 1
echo -n "2..."
sleep 1
echo -n "1..."
sleep 1
echo
echo

$@

echo
echo "The command has been executed !"